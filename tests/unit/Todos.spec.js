// Import the `mount()` method from Vue Test Utils
import { shallowMount } from "@vue/test-utils";
import Todos from "../../src/components/Todos";

const items = [{ text: "todo1", completed: false }];

describe("Todos.vue test", () => {
  it("check components", () => {
    const wrapper = shallowMount(Todos, {
      propsData: {
        items,
      },
    });

    expect(wrapper.find("div").exists()).toBe(true);
    expect(wrapper.find("ul").exists()).toBe(true);
    expect(wrapper.find("li").text()).toBe("todo1");
  });
});
