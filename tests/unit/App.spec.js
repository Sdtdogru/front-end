// Import the `mount()` method from Vue Test Utils
import { shallowMount } from "@vue/test-utils";
import App from "../../src/App";
import Todos from "../../src/components/Todos";

describe("App.vue test", () => {
  it("check components", () => {
    const wrapper = shallowMount(App);

    expect(wrapper.find(".title").exists()).toBe(true);
    expect(wrapper.find(".title").text()).toBe("My Todos App");
    expect(wrapper.find(".input").exists()).toBe(true);
    expect(wrapper.find(".button").exists()).toBe(true);
    expect(wrapper.find(".button").text()).toBe("Add Task");
    expect(wrapper.findComponent(Todos).exists()).toBe(true);
  });

  it("input reading", async () => {
    const wrapper = shallowMount(App, {
      data() {
        return {
          text: "",
        };
      },
    });

    expect((wrapper.find(".input").value = "run")).toBe("run");
  });


  it("checks if addTask() func is called", () => {
    const wrapper = shallowMount(App);

    const mockaddTodo = jest.fn();

    wrapper.setMethods({
      addTodo: mockaddTodo,
    });

    wrapper.find(".button").trigger("click");
    expect(mockaddTodo).toHaveBeenCalled();
  });
  
});
