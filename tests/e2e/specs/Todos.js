// https://docs.cypress.io/api/introduction/api.html

describe("Todos Action", () => {
  it("Get todos", () => {
    cy.visit("/");
  });

  it("Add todo", () => {
    cy.visit("/");
    cy.get("input.input").type("new task");
    cy.get("button.button").trigger("click");
    cy.get("li");
  });
});
