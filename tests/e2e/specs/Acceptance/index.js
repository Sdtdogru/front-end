import {And, Given, When,Then} from "cypress-cucumber-preprocessor/steps";

Given('I open todo project page', () => {
cy.visit("/")
});

When('I add "new todo" to the input box',()=>{
    cy.get("input.input").type("new task");
});

And('click the Add Task Button',()=>{
    cy.get("button.button").trigger("click");
});
Then('new list the todo',()=>{
    cy.get("li");
});
