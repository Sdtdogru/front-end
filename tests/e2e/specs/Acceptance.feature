Feature: Todo Main Page

  I want to open the todo project

  @focus
  Scenario: Opening todo project page
    Given I open todo project page
    When I add "new todo" to the input box
    And click the Add Task Button
    Then new list the todo
