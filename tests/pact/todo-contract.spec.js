const { Pact } = require("@pact-foundation/pact");
const { like, eachLike } = require("@pact-foundation/pact").Matchers;
import App from "../../src/App.vue";
const path = require("path");

const PORT = 8000;

const provider = new Pact({
  consumer: "TodoGoExample",
  provider: "TodoVueExample",
  port: PORT,
  log: path.resolve(process.cwd(), "logs", "pact.log"),
  dir: path.resolve(process.cwd(), "pacts"),
  logLevel: "INFO",
});

describe("Todos Service", () => {
  describe("When a request to list all todos is made", () => {
    beforeAll(() =>
        provider.setup().then(() => {
          provider.addInteraction({
            state: "check for todo",
            uponReceiving: "a request to list all todos",
            withRequest: {
              method: "GET",
              path: "/todo",
              headers: { "Content-Type": "application/json" },
            },
            willRespondWith: {
              status: 200,
              body: eachLike({
                text: like("Movie 1"),
              }),
              headers: { "Content-Type": "application/json" },
            },
          });
        })
    );

    test("should return the correct data", async () => {
      const response = await App.methods.getTodos();
      expect(response[0].text).toBe("Movie 1");
    });

    afterAll(() => provider.finalize());
  });
});
