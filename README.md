
## Todo List Project


### Add Todo.vue
``` bash

<template>
  <div>
    <ul>
      <li v-for="item in items" :key="item.index">
         {{ item.text }}
      </li>
    </ul>
  </div>
</template>

<script>
export default {
  name: "Todos",
  props: {
    items: Array,
  },
};
</script>

```

### Update template and add axios rest api
``` bash
<template>
  <div id="app">
    <h1 class="title">My Todos App</h1>
    <div class="inputs">
      <input
        v-model="text"
        class="input"
        type="text"
        placeholder="give me something"
      />
      <button @click="addTodo" class="button">Add Task</button>
    </div>
    <Todos :items="todos"></Todos>
  </div>
</template>

<script>
import Todos from "./components/Todos.vue";
import axios from "axios";

export default {
  name: "App",
  components: {
    Todos,
  },
  data() {
    return {
      text: "",
      todos: [],
    };
  },
  methods: {
    async addTodo() {
      if (!this.text) {
        return;
      }
      const response = await axios.post("http://localhost:3000/todo", {
        text: this.text,
      });
      if (response.status === 201) {
        this.getTodos();
        this.text = "";
      }
    },
    async getTodos() {
      const res = await axios.get("http://localhost:3000/todo");
      this.todos = res.data;
      console.log(this.todos);
      return this.todos;
    },
  },
  mounted() {
    this.getTodos();
  },
};
</script>
``` 

### npm run serve

![](./images/runserve.PNG)

### Created unit test
#### Unit test for Todo.vue
``` bash
describe("Todos.vue test", () => {
  it("check components", () => {
    const wrapper = shallowMount(Todos, {
      propsData: {
        items,
      },
    });

    expect(wrapper.find("div").exists()).toBe(true);
    expect(wrapper.find("ul").exists()).toBe(true);
    expect(wrapper.find("li").text()).toBe("todo1");
  });
});
``` 
#### Unit test for App.vue 
``` bash
describe("App.vue test", () => {
  it("check components", () => {
    const wrapper = shallowMount(App);

    expect(wrapper.find(".title").exists()).toBe(true);
    expect(wrapper.find(".title").text()).toBe("My Todos App");
    expect(wrapper.find(".input").exists()).toBe(true);
    expect(wrapper.find(".button").exists()).toBe(true);
    expect(wrapper.find(".button").text()).toBe("Add Task");
    expect(wrapper.findComponent(Todos).exists()).toBe(true);
  });

  it("input reading", async () => {
    const wrapper = shallowMount(App, {
      data() {
        return {
          text: "",
        };
      },
    });

    expect((wrapper.find(".input").value = "run")).toBe("run");
  });


  it("checks if addTask() func is called", () => {
    const wrapper = shallowMount(App);

    const mockaddTodo = jest.fn();

    wrapper.setMethods({
      addTodo: mockaddTodo,
    });

    wrapper.find(".button").trigger("click");
    expect(mockaddTodo).toHaveBeenCalled();
  });
  
});
```

### npm run test:unit
![](./images/unittest.PNG)

### E2E and Acceptance tests
#### Feature file
``` bash
Feature: Todo Main Page

  I want to open the todo project

  @focus
  Scenario: Opening todo project page
    Given I open todo project page
    When I add "new todo" to the input box
    And click the Add Task Button
    Then new list the todo

```

#### index.js file
``` bash
Given('I open todo project page', () => {
cy.visit("/")
});

When('I add "new todo" to the input box',()=>{
    cy.get("input.input").type("new task");
});

And('click the Add Task Button',()=>{
    cy.get("button.button").trigger("click");
});
Then('new list the todo',()=>{
    cy.get("li");
});

```

### npm run test:e2e
![](./images/acceptance.PNG)

### Cunsumer test 

``` bash
const { Pact } = require("@pact-foundation/pact");
const { like, eachLike } = require("@pact-foundation/pact").Matchers;
import App from "../../src/App.vue";
const path = require("path");

const PORT = 8000;

const provider = new Pact({
  consumer: "TodoGoExample",
  provider: "TodoVueExample",
  port: PORT,
  log: path.resolve(process.cwd(), "logs", "pact.log"),
  dir: path.resolve(process.cwd(), "pacts"),
  logLevel: "INFO",
});

describe("Todos Service", () => {
  describe("When a request to list all todos is made", () => {
    beforeAll(() =>
        provider.setup().then(() => {
          provider.addInteraction({
            state: "check for todo",
            uponReceiving: "a request to list all todos",
            withRequest: {
              method: "GET",
              path: "/todo",
              headers: { "Content-Type": "application/json" },
            },
            willRespondWith: {
              status: 200,
              body: eachLike({
                text: like("Movie 1"),
              }),
              headers: { "Content-Type": "application/json" },
            },
          });
        })
    );

    test("should return the correct data", async () => {
      const response = await App.methods.getTodos();
      expect(response[0].text).toBe("Movie 1");
    });

    afterAll(() => provider.finalize());
  });
});

```

### Cunsumer runing -npm run test:pact 

![](./images/consumer.PNG)

## Heroku deploy

![](./images/deploy.PNG)
 </br>

# Thank you for looking project :)
